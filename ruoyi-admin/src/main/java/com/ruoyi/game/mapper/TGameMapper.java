package com.ruoyi.game.mapper;

import java.util.List;
import com.ruoyi.game.domain.TGame;

/**
 * 游戏模型Mapper接口
 * 
 * @author ruoyi
 * @date 2020-04-19
 */
public interface TGameMapper 
{
    /**
     * 查询游戏模型
     * 
     * @param gameId 游戏模型ID
     * @return 游戏模型
     */
    public TGame selectTGameById(Long gameId);

    public TGame selectTGameByName(String gameName);
    /**
     * 查询游戏模型列表
     * 
     * @param tGame 游戏模型
     * @return 游戏模型集合
     */
    public List<TGame> selectTGameList(TGame tGame);

    /**
     * 新增游戏模型
     * 
     * @param tGame 游戏模型
     * @return 结果
     */
    public int insertTGame(TGame tGame);

    /**
     * 修改游戏模型
     * 
     * @param tGame 游戏模型
     * @return 结果
     */
    public int updateTGame(TGame tGame);

    /**
     * 删除游戏模型
     * 
     * @param gameId 游戏模型ID
     * @return 结果
     */
    public int deleteTGameById(Long gameId);

    /**
     * 批量删除游戏模型
     * 
     * @param gameIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteTGameByIds(String[] gameIds);
}
