package com.ruoyi.game.controller;

import java.util.List;

import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysUser;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.game.domain.TGame;
import com.ruoyi.game.service.ITGameService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 游戏模型Controller
 * 
 * @author ruoyi
 * @date 2020-04-19
 */
@Controller
@RequestMapping("/game/game")
public class TGameController extends BaseController
{
    private String prefix = "game/game";

    @Autowired
    private ITGameService tGameService;

    @RequiresPermissions("game:game:view")
    @GetMapping()
    public String game()
    {
        return prefix + "/game";
    }

    /**
     * 查询游戏模型列表
     */
    @RequiresPermissions("game:game:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(TGame tGame)
    {
        startPage();
        List<TGame> list = tGameService.selectTGameList(tGame);
        return getDataTable(list);
    }

    /**
     * 导出游戏模型列表
     */
    @RequiresPermissions("game:game:export")
    @Log(title = "游戏模型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(TGame tGame)
    {
        List<TGame> list = tGameService.selectTGameList(tGame);
        ExcelUtil<TGame> util = new ExcelUtil<TGame>(TGame.class);
        return util.exportExcel(list, "game");
    }

    /**
     * 新增游戏模型
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存游戏模型
     */
    @RequiresPermissions("game:game:add")
    @Log(title = "游戏模型", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(TGame tGame)
    {
        return toAjax(tGameService.insertTGame(tGame));
    }

    /**
     * 修改游戏模型
     */
    @GetMapping("/edit/{gameId}")
    public String edit(@PathVariable("gameId") Long gameId, ModelMap mmap)
    {
        TGame tGame = tGameService.selectTGameById(gameId);
        mmap.put("tGame", tGame);
        return prefix + "/edit";
    }

    /**
     * 修改保存游戏模型
     */
    @RequiresPermissions("game:game:edit")
    @Log(title = "游戏模型", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(TGame tGame)
    {
        return toAjax(tGameService.updateTGame(tGame));
    }

    /**
     * 删除游戏模型
     */
    @RequiresPermissions("game:game:remove")
    @Log(title = "游戏模型", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(tGameService.deleteTGameByIds(ids));
    }

    @PostMapping("/importData")
    @ResponseBody
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<TGame> util = new ExcelUtil<TGame>(TGame.class);
        List<TGame> gameList = util.importExcel(file.getInputStream());
        String operName = ShiroUtils.getSysUser().getLoginName();
        String message = tGameService.importGame(gameList,updateSupport,operName);
        return AjaxResult.success(message);
    }

    @GetMapping("/importTemplate")
    @ResponseBody
    public AjaxResult importTemplate()
    {
        ExcelUtil<TGame> util = new ExcelUtil<TGame>(TGame.class);
        return util.importTemplateExcel("游戏数据");
    }
}
