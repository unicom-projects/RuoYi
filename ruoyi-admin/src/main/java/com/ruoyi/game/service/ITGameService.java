package com.ruoyi.game.service;

import java.util.List;
import com.ruoyi.game.domain.TGame;

/**
 * 游戏模型Service接口
 * 
 * @author ruoyi
 * @date 2020-04-19
 */
public interface ITGameService 
{
    /**
     * 查询游戏模型
     * 
     * @param gameId 游戏模型ID
     * @return 游戏模型
     */
    public TGame selectTGameById(Long gameId);

    /**
     * 查询游戏模型列表
     * 
     * @param tGame 游戏模型
     * @return 游戏模型集合
     */
    public List<TGame> selectTGameList(TGame tGame);

    /**
     * 新增游戏模型
     * 
     * @param tGame 游戏模型
     * @return 结果
     */
    public int insertTGame(TGame tGame);

    /**
     * 修改游戏模型
     * 
     * @param tGame 游戏模型
     * @return 结果
     */
    public int updateTGame(TGame tGame);

    /**
     * 批量删除游戏模型
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTGameByIds(String ids);

    /**
     * 删除游戏模型信息
     * 
     * @param gameId 游戏模型ID
     * @return 结果
     */
    public int deleteTGameById(Long gameId);

    public String importGame(List<TGame> gameList, Boolean isUpdateSupport, String operName);
}
