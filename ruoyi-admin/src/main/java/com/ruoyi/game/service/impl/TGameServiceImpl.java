package com.ruoyi.game.service.impl;

import java.util.List;

import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.game.mapper.TGameMapper;
import com.ruoyi.game.domain.TGame;
import com.ruoyi.game.service.ITGameService;
import com.ruoyi.common.core.text.Convert;

/**
 * 游戏模型Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-04-19
 */
@Service
public class TGameServiceImpl implements ITGameService 
{
    private static final Logger log = LoggerFactory.getLogger(TGameServiceImpl.class);
    @Autowired
    private TGameMapper tGameMapper;

    /**
     * 查询游戏模型
     * 
     * @param gameId 游戏模型ID
     * @return 游戏模型
     */
    @Override
    public TGame selectTGameById(Long gameId)
    {
        return tGameMapper.selectTGameById(gameId);
    }


    /**
     * 查询游戏模型列表
     * 
     * @param tGame 游戏模型
     * @return 游戏模型
     */
    @Override
    public List<TGame> selectTGameList(TGame tGame)
    {
        return tGameMapper.selectTGameList(tGame);
    }

    /**
     * 新增游戏模型
     * 
     * @param tGame 游戏模型
     * @return 结果
     */
    @Override
    public int insertTGame(TGame tGame)
    {
        tGame.setCreateTime(DateUtils.getNowDate());
        return tGameMapper.insertTGame(tGame);
    }

    /**
     * 修改游戏模型
     * 
     * @param tGame 游戏模型
     * @return 结果
     */
    @Override
    public int updateTGame(TGame tGame)
    {
        tGame.setUpdateTime(DateUtils.getNowDate());
        return tGameMapper.updateTGame(tGame);
    }

    /**
     * 删除游戏模型对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTGameByIds(String ids)
    {
        return tGameMapper.deleteTGameByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除游戏模型信息
     * 
     * @param gameId 游戏模型ID
     * @return 结果
     */
    @Override
    public int deleteTGameById(Long gameId)
    {
        return tGameMapper.deleteTGameById(gameId);
    }

    @Override
    public String importGame(List<TGame> gameList, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(gameList) || gameList.size() == 0) {
            throw new BusinessException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();

        for (TGame game : gameList) {
            try {

                TGame g=tGameMapper.selectTGameByName(game.getGameName());
                if (StringUtils.isNull(g)) {
                    this.insertTGame(game);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + game.getGameName() + " 导入成功");
                }
                else if (isUpdateSupport) {
                    game.setUpdateBy(operName);
                    this.updateTGame(game);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + game.getGameName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + game.getGameName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、 " + game.getGameName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new BusinessException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}
