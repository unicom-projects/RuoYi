package com.ruoyi.game.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 游戏模型对象 t_game
 * 
 * @author ruoyi
 * @date 2020-04-19
 */
public class TGame extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long gameId;

    /** 游戏名称 */
    @Excel(name = "游戏名称")
    private String gameName;

    /** 游戏级别 */
    @Excel(name = "游戏级别")
    private Long gameLevel;

    /** 游戏评价 */
    @Excel(name = "游戏评价")
    private Long gameStar;

    public void setGameId(Long gameId) 
    {
        this.gameId = gameId;
    }

    public Long getGameId() 
    {
        return gameId;
    }
    public void setGameName(String gameName) 
    {
        this.gameName = gameName;
    }

    public String getGameName() 
    {
        return gameName;
    }
    public void setGameLevel(Long gameLevel) 
    {
        this.gameLevel = gameLevel;
    }

    public Long getGameLevel() 
    {
        return gameLevel;
    }
    public void setGameStar(Long gameStar) 
    {
        this.gameStar = gameStar;
    }

    public Long getGameStar() 
    {
        return gameStar;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("gameId", getGameId())
            .append("gameName", getGameName())
            .append("gameLevel", getGameLevel())
            .append("gameStar", getGameStar())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
